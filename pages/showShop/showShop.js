'use strict';

// 获取全局应用程序实例对象
var app = getApp()
var common = require('../common/common.js')
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: '商品列表',
    goodsList: [],
    page:0,
    lastPage:100,
    cid:0,
    inLoding:0,
    brandid:0,
    brandList:[{
      brand_id:0,
      brand_name:'全部'
    }],
    brandIndex:0,
    loadingMsg:'加载中',
    multiIndex:[0,0],
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function onLoad(e) {
    var _=this;
    console.log(e);
    this.setData(e);
    this.getGoodsList(e)
    // TODO: onLoad
    common.getGoodsCategory(
      function (res) {
        var catList = [{ cat_name: '全部', cid: 0 }];
        var setCatList = [{ cat_name: '全部', cid: 0 }];
        var setCatList2 = [{ cat_name: '全部', cid: 0 }];
        var cat0 = 1;
        var multiIndex = [0, 0]
        for (var i in res.data) {
          setCatList.push({ cat_name: res.data[i].cat_name, cid: res.data[i].cid })
          catList.push(res.data[i]);
          var cat1 = 0;
          if (parseInt(res.data[i].cid) == parseInt(e.cid)) {
            multiIndex = [cat0, cat1];
          }
          for (var x in res.data[i].children) {
            if (parseInt(res.data[i].children[x].cid) == parseInt(e.cid)) {
              multiIndex = [cat0, cat1];
              setCatList2 = res.data[i].children
            }
            cat1++;
          }
          cat0++;
        }
        _.setData({
          setCatList: [setCatList, setCatList2],
          cateList: catList,
          multiIndex: multiIndex
        })
      }
    )
  },
  /**
   * 用户滑动到底部触发
   */
  onReachBottom:function(){
    this.getGoodsList()
  },
  bindPickerChange: function (e) {
    var brandInfo = this.data.brandList[e.detail.value];
    this.setData({
      brandid:brandInfo.brand_id,
      brandIndex: e.detail.value,
      goodsList:[],
      page:0,
      lastPage:99,
      loadingMsg: '正在加载中~~~',
    });
    this.getGoodsList();
  },
  bindMultiPickerChange2: function (e) {
    var cateList = this.data.setCatList;
    var cid=0;
    if (e.detail.value[1] == 0) {
      cid = cateList[0][e.detail.value[0]].cid;
    } else {
      cid = cateList[1][e.detail.value[1]].cid;
    }
    if(cid==undefined){
      cid=0;
    }
    this.setData({
      cid: cid,
      goodsList: [],
      page: 0,
      lastPage: 99,
      loadingMsg: '正在加载中~~~',
    });
    this.getGoodsList();
  },
  bindMultiPickerColumnChange2: function (e) {
    var multiIndex = this.data.multiIndex;
    var cateList = this.data.setCatList;
    multiIndex[e.detail.column]=e.detail.value;
    if(e.detail.column==0){
      multiIndex[1]=0
      var datas = this.data.cateList[e.detail.value].children
      var setCatList = [];
      for (var i in datas) {
        setCatList.push(datas[i])
      }
      cateList[1]=setCatList;
    }
    this.setData({
      multiIndex: multiIndex,
      setCatList: cateList
    })
  },
  /**
   * 加载商品列表
   */
  getGoodsList:function(){
    var _ = this;
    var page=this.data.page;
    page++
    this.setData({
      page:page
    })
    if(page>this.data.lastPage){
      this.setData({
        loadingMsg:'没有更多商品了~~~',
        inLoding:1
        })
    } else {
      this.setData({
        inLoding: 1
      })
      var goodsList = _.data.goodsList;
      common.getGoodsPageList({ cid: _.data.cid, page: page, brand: _.data.brandid }, function (res) {
        goodsList = goodsList.concat(res.data.itemList.data);
        var brandList = [{ brand_id: 0, brand_name: '全部' }];
        var index = 1;
        var brandIndex = 0;
        for (var i in res.data.brandList) {
          if (parseInt(res.data.brandList[i]['brand_id']) == parseInt(_.data.brandid)) {
            brandIndex = index;
          }
          index++;
          brandList.push(res.data.brandList[i])
        }
        _.setData({
          lastPage: res.data.itemList.last_page,
          goodsList: goodsList,
          brandList: brandList,
          brandIndex: brandIndex,
          inLoding: 0
        })
        if (page >= _.data.lastPage) {
          _.setData({
            inLoding: 1,
            loadingMsg: '没有更多商品了~~~',
          })
        }
      });
    }
  }
});
//# sourceMappingURL=showShop.js.map
