'use strict';

// 获取全局应用程序实例对象
var app = getApp();
console.log(app.globalData)
var common=require('../common/common.js');
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'index',
    userInfo: null,
    /** 
    userSite: '定位中',
    */
    navList: [{
      navTitle: '分类',
      url: '../classify/index',
      navIcon: 'iconfont icon-shalou'
    }, {
      navTitle: '热卖',
      url: '../showShop/showShop?type={{ index }}',
      navIcon: 'iconfont icon-chuliyuyue'
    }, {
      navTitle: '新品',
      url: '../showShop/showShop?type={{ index }}',
      navIcon: 'iconfont icon-erweima'
    }, {
      navTitle: '推荐',
      url: '../showShop/showShop?type={{ index }}',
      navIcon: 'iconfont icon-erweima'
    },{
      navTitle: '促销',
      url: '../showShop/showShop?type={{ index }}',
      navIcon: 'iconfont icon-erweima'
    }, {
      navTitle: '品牌',
      url: '../brand/index',
      navIcon: 'iconfont icon-erweima'
    }, {
      navTitle: '我们',
      url: '../showShop/showShop?type={{ index }}',
      navIcon: 'iconfont icon-erweima'
    }, ],
    hotShop: [], //品牌
    nearShop: [],//推荐商品
      
    imgUrls: ['http://img02.tooopen.com/images/20150928/tooopen_sy_143912755726.jpg', 'http://img06.tooopen.com/images/20160818/tooopen_sy_175866434296.jpg', 'http://img06.tooopen.com/images/20160818/tooopen_sy_175833047715.jpg']
  },
  /**
   * 用户选择位置
   * @returns {boolean}
   */
  /**
  chooseLocation: function chooseLocation() {
    // console.log(1)
    var that = this;
    wx.chooseLocation({
      success: function success(res) {
        if (res.name.length <= 0) {
          return that.setData({
            userSite: res.address
          });
        }
        that.setData({
          userSite: res.name
        });
      }
    });
  },
  */
  /**
   * 用户搜索
   */
  goSearch: function goSearch() {
    wx.navigateTo({
      url: '../search/search'
    });
  },

  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function onLoad() {
    var _this = this;
    wx.getLocation({
      success: function success(res) {
        console.log(res);
      }
    });
    common.getTagGoodsList(['is_commend'], 5, 0, function (res) {
      _this.setData({
        imgUrls: res.data
      })
    });
    common.getTagGoodsList(['is_commend'],10,0,function(res){
      _this.setData({
        nearShop: res.data
      })
    });
    common.getBrandList(0, 6,function(res){
      _this.setData({
        hotShop: res.data
      })
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
  */
  onReady: function onReady() {
    console.log(' ---------- onReady ----------');
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function onShow() {
    console.log(' ---------- onShow ----------');
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function onHide() {
    console.log(' ---------- onHide ----------');
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function onUnload() {
    console.log(' ---------- onUnload ----------');
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function onPullDownRefresh() {
    console.log(' ---------- onPullDownRefresh ----------');
  },
  
});
//# sourceMappingURL=index.js.map
