'use strict';

// 获取全局应用程序实例对象
 const app = getApp()

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'search',
    nearShop: [],
    searchText: null,
    history: [],
    chooseHistory: null,
    searchShow: true,
    page:0,
    lastPage:99,
  },
  /**
   * 清空搜索记录
   */
  cleanHistory: function cleanHistory() {
    this.setData({
      history: [],
      searchShow: false
    });
    wx.removeStorageSync('history');
  },

  /**
   * 改变标签选择
   * @param e
   */
  chooseTip: function chooseTip(e) {
    var index = e.currentTarget.dataset.choose;
    this.setData({
      chooseHistory: index
    });
  },

  /**
   * 搜索返回
   */
  searchShop: function searchShop(e) {
    var searcheText = this.data.searchText;
    var that = this;
    var history = that.data.history;
    if (!history) {
      history = [searcheText];
      that.setData({
        history:history
      })
      that.data.history = history;
    } else {
      var count = history.unshift(searcheText);
      if (count >= 10) {
        that.data.history.pop();
      }
    }
    this.setData({
      chooseHistory: 0,
      searchShow: true,
      page:0,
      lastPage:99,
      nearShop:[]
    });
    wx.setStorage({
      key: 'history',
      data: that.data.history,
      success: function success() {
        that.setData({
          history: wx.getStorageSync('history')
        });
      }
    });
    this.getGoodsList();
  },

  /**
   * 键盘输入改变搜索结果
   */
  searchInput: function searchInput(e) {
    this.setData({
      searchText: e.detail.value
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function onLoad() {
    // 读取搜索历史
    var history = wx.getStorageSync('history');
    if (!history) {
      this.setData({
        searchShow: false
      });
    }
    this.setData({
      history: history
    });
    // TODO: onLoad
  },
  /**
 * 加载商品列表
 */
  getGoodsList: function () {
    var _ = this;
    var page = this.data.page;
    page++
    this.setData({
      page: page
    })
    if (page > this.data.lastPage) {
      this.setData({
        loadingMsg: '没有更多商品了~~~',
        inLoding: 1
      })
    } else {
      this.setData({
        inLoding: 1
      })
      var nearShop = _.data.nearShop;
      wx.request({
        url: app.d.hostUrl + '/home/Goods/search',
        method: 'POST',
        data: { page: page, goods_name: _.data.searchText},
        success: function (res) {
          nearShop = nearShop.concat(res.data.data);
          var brandList = [{ brand_id: 0, brand_name: '全部' }];
          var index = 1;
          var brandIndex = 0;
          for (var i in res.data.brandList) {
            if (parseInt(res.data.brandList[i]['brand_id']) == parseInt(_.data.brandid)) {
              brandIndex = index;
            }
            index++;
            brandList.push(res.data.brandList[i])
          }
          _.setData({
            lastPage: res.data.last_page,
            nearShop: nearShop,
            inLoding: 0
          })
          if (page >= _.data.lastPage) {
            _.setData({
              inLoding: 1,
              loadingMsg: '没有更多商品了~~~',
            })
          }
        }
      })
    }
  }
});
//# sourceMappingURL=search.js.map
