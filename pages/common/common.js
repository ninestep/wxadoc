var app=getApp();
/**
 * 获取商品分页
 */
function getGoodsPageList(data,reback) {
  wx.request({
    url: app.d.hostUrl + '/home/Goods/itemlist',
    method: 'POST',
    data: data,
    success: function(res){
      reback(res);
    },
    fail: function () {
      wx.showToast({
        title: '网络出错',
        icon: 'success',
        duration: 2000
      })
    }
  })
}
module.exports.getGoodsPageList = getGoodsPageList
/**
 * 按照是否推荐（is_commend），热门（is_hot），新品（is_new），精品（is_best）获取商品
 * tag以数组形式传入，例如['is_commend','is_hot']
 */
function getTagGoodsList(tag, limit, cid = 0, reback = function (res) { console.log(res) }) {
  wx.request({
    url: app.d.hostUrl + '/home/Goods/getTagGoodsList',
    method: 'POST',
    data: { tag: tag, limit: limit, cid: cid },
    success: function (res) {
      reback(res);
    },
    fail: function () {
      wx.showToast({
        title: '网络出错',
        icon: 'success',
        duration: 2000
      })
    }
  })
}
module.exports.getTagGoodsList = getTagGoodsList
/**
 * 获取产品分类
 */
function getGoodsCategory(reback=function(res){console.log(res)}){
  wx.request({
    url: app.d.hostUrl + '/home/Goods/getCategory',
    method: 'POST',
    success: function (res) {
      reback(res);
    },
    fail: function () {
      wx.showToast({
        title: '网络出错',
        icon: 'success',
        duration: 2000
      })
    }
  })
}
module.exports.getGoodsCategory = getGoodsCategory
/**
 * 获取品牌列表
 */
function getBrandList(cid=0,limit=9,reback=function(res){console.log(res)}){
  var back=[];
  wx.request({
    url: app.d.hostUrl + '/home/Goods/getBrandList',
    method: 'POST',
    data: {cid:cid,limit:limit},
    header: {
      'Accept': 'application/json'
    },
    success: function (res) {
      reback(res);
    },
    fail: function () {
      wx.showToast({
        title: '网络出错',
        icon: 'success',
        duration: 2000
      })
    }
  })
  return back;
}
module.exports.getBrandList = getBrandList
/**
 * 获取品牌列表分页
 */
function getBrandPageList(data={},reback=function(res){console.log(res)}){
  wx.request({
    url: app.d.hostUrl + '/home/Goods/getBrandPageList',
    method: 'POST',
    data: data,
    header: {
      'Accept': 'application/json'
    },
    success: function (res) {
      reback(res);
    },
    fail:function(){
      wx.showToast({
        title: '网络出错',
        icon: 'success',
        duration: 2000
      })
    }
  })
}
module.exports.getBrandPageList = getBrandPageList
/**
 * 获取订单列表
 * status 订单状态 0：全部订单，1待付款，2：待发货，3：待收货，4：待评价
 * map 筛选条件
 */
function getOrderList(status,reback,map={}){
  var sessinoKey = wx.getStorageSync(session_key);
  var data = { status: status, session_key: sessinoKey };
  data=Object.assign(data,map);
  wx.request({
    url: app.d.hostUrl+'/home/Order/index',
    data: { status: status, session_key:sessinoKey},
    success:function(res){
        reback(res)
    },
    fail: function () {
      wx.showToast({
        title: '网络出错',
        icon: 'success',
        duration: 2000
      })
    }
  })
}
