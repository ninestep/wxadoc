var app = getApp()
Page( {
  data: {
    userInfo: {},
    projectSource: 'https://github.com/liuxuanqiang/wechat-weapp-mall',
    userListInfo: [ {
      icon: '../../images/iconfont-dingdan.png',
      text: '我的订单',
      isunread: true,
      unreadNum: 2,
      url: '../my/dingdan?currentTab=0&otype=pay'
    }, {
        icon: '../../images/iconfont-card.png',
        text: '我的积分',
        isunread: false,
        unreadNum: 2,
        url: '../my/dingdan?currentTab=0&otype=pay'
      }, {
        icon: '../../images/iconfont-icontuan.png',
        text: '我的评价',
        isunread: true,
        unreadNum: 1,
        url: '../my/dingdan?currentTab=0&otype=pay'
    }, {
      icon: '../../images/iconfont-icontuan.png',
      text: '我的收藏',
      isunread: true,
      unreadNum: 1,
      url: '../my/shoucang'
    }, {
        icon: '../../images/iconfont-shouhuodizhi.png',
        text: '收货地址管理',
        url: '../address/user-address/user-address'
      }, {
        icon: '../../images/iconfont-kefu.png',
        text: '联系客服',
        url: '../my/dingdan?currentTab=0&otype=pay'
      }, {
        icon: '../../images/iconfont-help.png',
        text: '常见问题',
        url: '../my/dingdan?currentTab=0&otype=pay'
      }],
      memberNav: [{
        icon: '../../images/dfk.png',
        text: '待付款',
        isunread: true,
        unreadNum: 15,
        url:'../my/dingdan?currentTab=0&otype=pay'
      }, {
        icon: '../../images/dpl.png',
        text: '待收货',
        isunread: true,
        unreadNum: 1,
        url: '../my/dingdan?currentTab=2&otype=receive'
      }, {
        icon: '../../images/dsh.png',
        text: '已完成',
        isunread: true,
        unreadNum: 1,
        url: '../my/dingdan?currentTab=3&otype=finish'
      }, {
        icon: '../../images/tksh.png',
        text: '退款/售后',
        isunread: true,
        unreadNum: 1,
        url: '../my/dingdan?currentTab=4'
      }]
  },

  onLoad: function() {
    var that = this
    //调用应用实例的方法获取全局数据
    app.getUserInfo( function( userInfo ) {
      //更新数据
      that.setData( {
        userInfo: userInfo
      })
    })
  }
})