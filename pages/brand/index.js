var app = getApp();
var common = require('../common/common.js');
Page({
  data: {
    page: 1,
    lastPage:99,
  },
  onLoad: function (options) {
    var that = this
    var page = this.data.page;
    var data = { page: page };
    common.getBrandPageList(data, function (res) {
      page++;
      that.setData({
        brandList: res.data.data,
        page: page
      });
    })
  },
  /**
* 用户滑动到底部触发
*/
  onReachBottom: function () {
    var that = this
    var page = this.data.page;
    var lastPage = this.data.lastPage;
    var data = { page: page };
    if(lastPage<page){
      common.getBrandPageList(data, function (res) {
        page++;
        that.setData({
          brandList: res.data.data,
          page: page,
          lastPage: res.data.last_page
        });
      })
    }
  },
})