// app.js
App({
  d: {
    hostUrl: 'https://swx.52phper.cn',
    hostImg: 'http://img.ynjmzb.net',
    hostVideo: 'http://zhubaotong-file.oss-cn-beijing.aliyuncs.com',
    userId: 1,
    appId: "",
    appKey: "",
    ceshiUrl: 'https://wxplus.paoyeba.com/index.php',
  },
  onLaunch: function () {
    //调用API从本地缓存中获取数据
    var _=this;
    this.getUserInfo();
  },
  getUserInfo: function (cb) {
    var that = this;
    var session=null;
    wx.getStorage({
      key: 'session_key',
      success: function(res) {
        session=res.data;
      },
    })
    if (!session) {
      typeof cb == "function" && cb(this.globalData.userInfo)
    } else {
      //调用登录接口
      wx.login({
        success: function (res) {
          var code = res.code;
          //get wx user simple info
          wx.getUserInfo({
            success: function (res) {
              that.globalData.userInfo = res.userInfo
              typeof cb == "function" && cb(that.globalData.userInfo);
              //get user sessionKey
              //get sessionKey
              that.getUserSessionKey(code);
            }
          });
        }
      });
    }
  },

  getUserSessionKey: function (code) {
    //用户的订单状态
    var that = this;
    wx.request({
      url: that.d.hostUrl + '/home/Public/wxUserLogin',
      method: 'post',
      data: {
        code: code
      },
      success: function (res) {
        //--init data        
        var data = res.data.data;
        if (data.status == 0) {
          wx.showToast({
            title: data.msg,
            duration: 2000
          });
          return false;
        }
        wx.setStorage({
          key: 'session_key',
          data: data.sessionKey,
        })
        wx.setStorage({
          key: 'openid',
          data: data.openid,
        })
        //that.onLoginUser();
      },
      fail: function (e) {
        wx.showToast({
          title: '网络异常！err:getsessionkeys',
          duration: 2000
        });
      },
    });
  },
  onLoginUser: function () {
    var that = this;
    var user = that.globalData.userInfo;
    wx.request({
      url: that.d.hostUrl + '/home/Public/getUserInfo',
      method: 'post',
      data: {
        SessionId: user.sessionId,
        gender: user.gender,
        NickName: user.nickName,
        HeadUrl: user.avatarUrl,
        openid: user.openid
      },
      header: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function (res) {
        //--init data        
        var data = res.data.arr;
        var status = res.data.status;
        if (status != 1) {
          wx.showToast({
            title: res.data.err,
            duration: 3000
          });
          return false;
        }
        that.globalData.userInfo['id'] = data.ID;
        that.globalData.userInfo['NickName'] = data.NickName;
        that.globalData.userInfo['HeadUrl'] = data.HeadUrl;
        var userId = data.ID;
        if (!userId) {
          wx.showToast({
            title: '登录失败！',
            duration: 3000
          });
          return false;
        }
        that.d.userId = userId;
      },
      fail: function (e) {
        wx.showToast({
          title: '网络异常！err:authlogin',
          duration: 2000
        });
      },
    });
  },
  getOrBindTelPhone: function (returnUrl) {
    var user = this.globalData.userInfo;
    if (!user.tel) {
      wx.navigateTo({
        url: 'pages/binding/binding'
      });
    }
  },

  globalData: {
    userInfo:null
  },

  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  }

});